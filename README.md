# Phyto_Confocal_2023

## Installation

- [ ] [Download and install](https://imagej.net/software/fiji/) Fiji (https://imagej.net/software/fiji/)
- [ ] [Download and install](https://github.com/sujaypatil96/3D-object-counter/blob/master/3d-oc_.jar) 3D plugin (https://github.com/sujaypatil96/3D-object-counter/blob/master/3d-oc_.jar)

Drag the download file "3d-oc_.jar" to the Fiji plugins folder

*Remark* : a copy of this file is stored in the folder name "plugin" on this gitlab  

## Usage

- Launch Fiji
- Open and run "First_Step_ReadAndReduceHyperstack.ijm" script.

This script allows to determine the z limits of each 3d image

- Open and run "Second_Step_DrawAndAnalyze.ijm" script.

The first step in this script is manual, it uses segmentation-editor.

To learn at use segmentation-editor, please, read this [page](https://imagej.net/plugins/segmentation-editor)

The second step (compute 3d volume) is automatic.

- Once the script is finished in the results folder, there is a file with the saved results and a folder with illustration images.

# Demo video
A video to illustrate application was showed : 
[Read](https://git.unicaen.fr/nicolas.elie/phyto_confocal_2023/-/blob/master/phyto_confocal_2023.webm)
