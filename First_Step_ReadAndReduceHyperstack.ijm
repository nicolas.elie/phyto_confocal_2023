// Nicolas ELIE
// Université de Caen Normandie
// Reduce Z HyperStack
// Allows to remove z-slides at the beginning and at the end of the image stack without touching the organization of the channels
  macro "First_Step_ReadAndReduceHyperstack.ijm" {
    requires("1.50");

    // deletes all the images in ImageJ
    run("Close All");
    // delete all ImageJ windows
    Winlist = getList("window.titles");
    if (Winlist.length>0){
        for (i=0; i<Winlist.length; i++){
            selectWindow(Winlist[i]);
            if(Winlist[i]!="First_Step_ReadAndReduceHyperstack.ijm"){
                run("Close");
            } 
        }
    }



    // open a dialog box to select the directory where the images are located
    path=getDirectory("Choose a Directory");
    //creation of a list table containing all the files present in the current directory
    list = getFileList(path);

/*********************************************************************************
*           For backup
*    Creation of an OibReduce folder which contains the images in tif format
*    after deleting the slides of the original file in oib format
* *******************************************************************************/
   
    
    // Creation of the OibReduce folder
        // path definition
    myDir = path+"OibReduce"+File.separator;
        // record path
    File.makeDirectory(myDir);
        // Test creation
    if (!File.exists(myDir))
        exit("Unable to create directory");
    
    // Start loop; Browse directory
    for (ii=0; ii<list.length; ii++) {
         IJ.redirectErrorMessages();
        // Open only oib files - Olympus 
        if (endsWith(list[ii], "oib")){            
            /***********************************************************
            //   opening image 
            ********************************************************** */
            val=lastIndexOf(list[ii], ".");
            root=substring(list[ii],0,val)+".tif";            
            if (!File.exists(path+"/OibReduce/"+root)){
                run("Bio-Formats Macro Extensions");
                Ext.openImagePlus(path+list[ii]);
                Ext.close()

            if (!Stack.isHyperStack) exit ("HyperStack required");

            // Number of Channels
            Stack.getDimensions(width, height, channels, slices, frames) ;
            Dialog.createNonBlocking("Reduce Z HyperStack");
            Dialog.addMessage("Indicate valide range slices");
            Dialog.addNumber("Start", 1)
            Dialog.addNumber("End", nSlices/channels)
            Dialog.show;

            // retrieve the numbers indicated
            start=Dialog.getNumber();
            end=Dialog.getNumber();

            // Calculate the number of planes expected in the end
            nFinal=((end-start)*channels)+channels;


            //remove first slides
            for(i=1;i<start;i++){
                setSlice(1);
                run("Delete Slice", "delete=slice");
            }

            
            //remove last slides
            
            // if there is no or no more deletion to be done, end of the process
            if(nFinal!=nSlices){
                // as long as the desired number is not reached, delete z-slides
                do {
                    setSlice(nSlices);
                    run("Delete Slice", "delete=slice");
                } while (nSlices!=nFinal);
            }
            run("16-bit");
            saveAs("Tiff", path+"/OibReduce/"+root);
            run("Close");                   
}
}
}
waitForUser("Information", "End of procedure");
}
