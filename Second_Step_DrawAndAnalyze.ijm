// Nicolas ELIE
// Université de Caen Normandie
 
macro "Second_Step_DrawAndAnalyze" {
/* Program developed for analyzing 3D images acquired with a confocal microscope
      * The images are composed of 3 channels:
      * - Nile Red
      * - Chloropyl
      * - DIC: Differential Interference Contrast
      * Each channel is acquired in 3D.
      *
      * PREREQUISITE: The orginal images oib are read by the First_Step_ReadAndReduceHyperstack program
      * This program reads orginal images oib and backups in tif form keeping the z of interest.
      *
      * STEP 1: Draw for each algae the boundaries of its volume using of the Segmentations Editor tool
      * STEP 2: the program calculates the volume of the algaes
      * For step 3 and 4, segmentation is automatic.
      * STEP 3: the program calculates the entire volume of chlorophyll in the volume of the algae
      * STEP 4: the program calculates the entire volume of nile red in the volume of the algae
      * STEP 5: an Excel table with the data is saved.
      *
      */

     //Hide or not the images during the analysis
     //if the images are hidden the program goes faster
     HideImage=true;

    // efface toutes les images présentes dans ImageJ
    run("Close All");
    // efface toutes les fenetres ImageJ "non-images"
    Winlist = getList("window.titles");
    if (Winlist.length>0){
        for (i=0; i<Winlist.length; i++){
            selectWindow(Winlist[i]);
            if(Winlist[i]!="Second_Step_DrawAndAnalyze.ijm"){
                run("Close");
            } 
        }
    }

  
    // open a dialog box to select the directory where the images are located
    path=getDirectory("Choose a Directory");
    //creation of a list table containing all the files present in the current directory
    list = getFileList(path);

   /*******************************************************************
    * For the backup 
    * Results : contains 1 folder and the files readable without ImageJ
    * Images4J : images obtained with ImageJ and readable with ImageJ
    * ******************************************************************/
    
    
    // Creation of Results folder
        // path
    myDir = path+"results"+File.separator;
        // write folder
    File.makeDirectory(myDir);
        // Test creation
    if (!File.exists(myDir))
        exit("Unable to create directory");


    // Creation of Images4J folder
        // path
    myDir = path+"results/Images4J"+File.separator;
        // ecriture du repertoire
        // write folder
    File.makeDirectory(myDir);
        // Test creation
    if (!File.exists(myDir))
        exit("Unable to create directory");

    setBatchMode(!HideImage);
    for (ii=0; ii<list.length; ii++) {
         IJ.redirectErrorMessages();
        // Open only oib files - Olympus 
        if (endsWith(list[ii], "oib")){            
            /***********************************************************
            //   opening image 
            ********************************************************** */
            val=lastIndexOf(list[ii], ".");
            root=substring(list[ii],0,val)+".tif";            
            // open images without masks
            if (!File.exists(path+root)){
                open(path+"/OibReduce/"+root);
            	run("Split Channels");
            	selectWindow("C1-"+root);
            	run("Close");
            	selectWindow("C2-"+root);
            	run("Close");
            	selectWindow("C3-"+root);
            	// create a new Segmentation Editor
				call("Segmentation_Editor.newSegmentationEditor");
				 
				// Reset the material list
				call("Segmentation_Editor.newMaterials");
				// Add a desired materials
				call("Segmentation_Editor.addMaterial", "Mask_Algue", 255, 255, 255);
                setTool("brush");
                waitForUser("Drawing to determine the volume of the algae","Draw the contours of the algae,\nnot necessarily on all slices,\nThen press I to interpolate between slices\nClick on  + to send these surfaces to the 3D image.\nOnce finished, you must close the drawing window\nby pressing the cross at the top right, then press OK\non this dialog box, respecting the order of these instructions");




                //run("16-bit");
                saveAs("Tiff", path+"/"+root);
                run("Close");                   
  
            }
        }
    }

    // Mask the images during the script operations
    //allows the program to go faster and use less memory
    setBatchMode(HideImage); 
    Nbalgaes=0;
    for (ii=0; ii<list.length; ii++) {
         IJ.redirectErrorMessages();
        // Open only oib files - Olympus 
        if (endsWith(list[ii], "oib")){            
            /***********************************************************
            //   opening image 
            ********************************************************** */
            val=lastIndexOf(list[ii], ".");
            root=substring(list[ii],0,val)+".tif"; 
            open(path+"/OibReduce/"+root);
            getVoxelSize(width, height, depth, unit);
            run("Split Channels");

            /**********************************************************
                        Volume 
            ************************************************************/
  
            nSelectionPerso=0;
            open(path+root);
            rename(list[ii]+"_Maskalgaes.tif");

			Stack.getStatistics(voxelCount, mean, min, max, stdDev);
			nSelectionPerso = max;
            selectWindow(list[ii]+"_Maskalgaes.tif");
            run("Duplicate...", "title=temp2 duplicate");

            selectWindow(list[ii]+"_Maskalgaes.tif");
			getVoxelSize(width, height, depth, unit);
			ResoW=width;
			ResoH=height;
            
            //Compute dimensions
			run("Set 3D Measurements",  "volume nb_of_obj._voxels centroid bounding_box close_original_images_while_processing_(saves_memory) dots_size=10 font_size=24 redirect_to=none");          

			volumeC3 = newArray(nSelectionPerso);

			for (y=1;y<nSelectionPerso+1;y++){
				selectWindow("temp2");
            	Stack.getDimensions(width,height,channels,slices,frames);
				for (yi = 1; yi < slices+1; yi++) {
					Stack.setSlice(yi);
					setThreshold(y, y);
					run("Create Mask");
					if (yi == 1) {
						mask = getImageID();
						selectImage(mask);
						rename("maskStack");
					} else if (yi == 2) {
						selectImage(mask);
						run("Concatenate...", "  title=[Concatenated Stacks] image1=maskStack image2=mask image3=[-- None --]");
					} else {
						run("Concatenate...", "  title=[Concatenated Stacks] image1=[Concatenated Stacks] image2=mask image3=[-- None --]");
					}
					selectImage("temp2");
				} 
				
			
				selectImage("Concatenated Stacks");
				setVoxelSize(ResoW, ResoH, depth, unit);
				run("3D object counter...", "threshold=1 slice=13 min.=100 max.=27262976 objects statistics");           
				

		            for (row=0; row<nResults; row++) {
			            volumeC3[y-1]=getResult("Volume (microns^3)", row)+volumeC3[y-1];                  
		            }

		            selectWindow("Results");
		            run("Close");
		            selectWindow("Objects map of Concatenated Stacks");
		            run("Close");
			}

			
            
            /**********************************************************
                        Volume Plasts : IMAGE Channel 2
            ************************************************************/
           /* Settings change for each image
            * Search for the most intense slice: mean intensity
            * the program is placed on this slice
            * Compute the automatic threshold with Otsu on this slice
            * Propagate this threshold to all slice
            */
            selectWindow("C2-"+root);
            run("Gaussian Blur...", "sigma=2");
            selectWindow("C2-"+root);
            run("Unsharp Mask...", "radius=2 mask=0.8");
            selectWindow("C2-"+root);
            run("Clear Results");

            run("Set Measurements...", "mean min display redirect=None decimal=2");
            run("Measure Stack...");
            //remove 0.2% start and end slices, risk of artifacts
            start=round(nResults*0.2);
            fin=nResults-round(nResults*0.2);
            max=0;
            indicemax=0;
            for (compteur=start; compteur<fin; compteur++) {		
                if (getResult("Mean", compteur)>max){
                    max=getResult("Mean", compteur);
                    indicemax=compteur;
                }		
            }

            setSlice(indicemax+1);
            setAutoThreshold("Otsu dark");
            run("Convert to Mask", "  black");
            selectWindow("Results");
            run("Close");

            run("Set 3D Measurements",  "volume nb_of_obj._voxels centroid bounding_box close_original_images_while_processing_(saves_memory) dots_size=10 font_size=24 redirect_to=none");

			// work on individual algaes.

			volumeC2= newArray(nSelectionPerso);		
			for (y=1;y<nSelectionPerso+1;y++){
				selectWindow("temp2");
            	Stack.getDimensions(width,height,channels,slices,frames);
				for (yi = 1; yi < slices+1; yi++) {
					Stack.setSlice(yi);
					setThreshold(y, y);
					run("Create Mask");
					if (yi == 1) {
						mask = getImageID();
						selectImage(mask);
						rename("maskStack");
					} else if (yi == 2) {
						selectImage(mask);
						run("Concatenate...", "  title=[Concatenated Stacks] image1=maskStack image2=mask image3=[-- None --]");
					} else {
						run("Concatenate...", "  title=[Concatenated Stacks] image1=[Concatenated Stacks] image2=mask image3=[-- None --]");
					}
					selectImage("temp2");
				} 
				
			
				selectImage("Concatenated Stacks");
				run("Subtract...", "value=254 stack");
				imageCalculator("Multiply create stack", "C2-"+root,"Concatenated Stacks");		
				Stack.getStatistics(voxelCount, mean, min, max, stdDev);

				if(max>0){ 
		            run("3D object counter...", "threshold=128 slice=13 min.=100 max.=27262976 objects statistics");
		            
		            run("Enhance Contrast", "saturated=0.35");
		            resetMinAndMax();	
		            setMinAndMax(0, nResults+1);
		            call("ij.ImagePlus.setDefault16bitRange", nResults+1);	            

		            saveAs("Tiff", path+"results/Images4J/"+list[ii]+"_"+y+"_Plasts.tif");
		            
		            for (row=0; row<nResults; row++) {
			            volumeC2[y-1]=getResult("Volume (micron^3)", row)+volumeC2[y-1];                  
		            }
		            
		            selectWindow("Results");
		            run("Close");
		        }else {
		        saveAs("Tiff", path+"results/Images4J/"+list[ii]+"_"+y+"_Plasts.tif");
		        	volumeC2[y-1]=0+volumeC2[y-1];
		        }
		       

	            selectWindow("Concatenated Stacks");
	            run("Close");
             
            }


            for(m=2;m<nSelectionPerso+1;m++){
            	imageCalculator("OR stack", list[ii]+"_1_Plasts.tif",list[ii]+"_"+m+"_Plasts.tif");          	
            	}
            selectImage(list[ii]+"_1_Plasts.tif");
			rename("Plasts");
			selectWindow("Plasts");
        	Stack.getDimensions(width,height,channels,slices,frames);
			for (yi = 1; yi < slices+1; yi++) {
				Stack.setSlice(yi);
				setThreshold(1, 255);
				run("Create Mask");
				if (yi == 1) {
					mask = getImageID();
					selectImage(mask);
					rename("maskStack");
				} else if (yi == 2) {
					selectImage(mask);
					run("Concatenate...", "  title=[Concatenated Stacks] image1=maskStack image2=mask image3=[-- None --]");
				} else {
					run("Concatenate...", "  title=[Concatenated Stacks] image1=[Concatenated Stacks] image2=mask image3=[-- None --]");
				}
				selectImage("Plasts");
			} 
			selectImage("Plasts");
			run("Close");
			selectImage("Concatenated Stacks");			
        	rename("Plasts");
                   

            /**********************************************************
                        NILE RED - VOLUME : IMAGE CHANNEL 1
                Volume of lipid structures stained with Nile Red
            ***********************************************************/

          	selectWindow("C1-"+root);
            run("Gaussian Blur...", "sigma=2");
            run("Unsharp Mask...", "radius=2 mask=0.8");


            selectWindow("C1-"+root);
            run("Clear Results");

            run("Set Measurements...", "mean min display redirect=None decimal=2");
            run("Measure Stack...");
            start=round(nResults*0.2);
            fin=nResults-round(nResults*0.2);
            max=0;
            indicemax=0;
            for (compteur=start; compteur<fin; compteur++) {		
                if (getResult("Mean", compteur)>max){
                    max=getResult("Mean", compteur);
                    indicemax=compteur;
                }		
            }

            setSlice(indicemax+1);
	
            setAutoThreshold("Otsu dark");
            run("Convert to Mask", "  black");
            selectWindow("Results");
            run("Close");

            //run("Set Measurements",       "volume nb_of_obj._voxels centroid bounding_box close_original_images_while_processing_(saves_memory) dots_size=10 font_size=24 redirect_to=none");
            run("Set 3D Measurements",  "volume nb_of_obj._voxels centroid bounding_box close_original_images_while_processing_(saves_memory) dots_size=10 font_size=24 redirect_to=none");


			// work on individual algaes.

			volumeC1= newArray(nSelectionPerso);		
			for (y=1;y<nSelectionPerso+1;y++){
				selectWindow("temp2");
            	Stack.getDimensions(width,height,channels,slices,frames);
				for (yi = 1; yi < slices+1; yi++) {
					Stack.setSlice(yi);
					setThreshold(y, y);
					run("Create Mask");
					if (yi == 1) {
						mask = getImageID();
						selectImage(mask);
						rename("maskStack");
					} else if (yi == 2) {
						selectImage(mask);
						run("Concatenate...", "  title=[Concatenated Stacks] image1=maskStack image2=mask image3=[-- None --]");
					} else {
						run("Concatenate...", "  title=[Concatenated Stacks] image1=[Concatenated Stacks] image2=mask image3=[-- None --]");
					}
					selectImage("temp2");
				} 
				
			
				selectImage("Concatenated Stacks");
				run("Subtract...", "value=254 stack");
				imageCalculator("Multiply create stack", "C1-"+root,"Concatenated Stacks");		

				Stack.getStatistics(voxelCount, mean, min, max, stdDev);

				if(max>0){ 
		            run("3D object counter...", "threshold=128 slice=1 min.=100 max.=1831308 objects statistics summary");
		            
		            //run("Brightness/Contrast...");
		            run("Enhance Contrast", "saturated=0.35");
		            resetMinAndMax(); 
		
		            setMinAndMax(0, nResults+1);
		            call("ij.ImagePlus.setDefault16bitRange", nResults+1);
	
		            
		            saveAs("Tiff", path+"results/Images4J/"+list[ii]+"_"+y+"_Lipids.tif");
		            
		            for (row=0; row<nResults; row++) {
			            volumeC1[y-1]=getResult("Volume (micron^3)", row)+volumeC1[y-1];                  
		            }
		            
		            selectWindow("Results");
		            run("Close");
				}else {
					saveAs("Tiff", path+"results/Images4J/"+list[ii]+"_"+y+"_Lipids.tif");
					volumeC1[y-1]=0+volumeC1[y-1];
				}
	            selectWindow("Concatenated Stacks");
	            run("Close");
	            //selectWindow(list[ii]+"_"+y+"_Lipides.tif");
	            //run("Close");  	             
            }
            //selectWindow("C1-"+root);                     
            //run("Close"); 
            for(n=2;n<nSelectionPerso+1;n++){
            	imageCalculator("Add stack", list[ii]+"_1_Lipids.tif",list[ii]+"_"+n+"_Lipids.tif");          	
            	}
            selectWindow(list[ii]+"_1_Lipids.tif");
			rename("Lipids");
			selectWindow("Lipids");
        	Stack.getDimensions(width,height,channels,slices,frames);
			for (yi = 1; yi < slices+1; yi++) {
				Stack.setSlice(yi);
				setThreshold(1, 255);
				run("Create Mask");
				if (yi == 1) {
					mask = getImageID();
					selectImage(mask);
					rename("maskStack");
				} else if (yi == 2) {
					selectImage(mask);
					run("Concatenate...", "  title=[Concatenated Stacks] image1=maskStack image2=mask image3=[-- None --]");
				} else {
					run("Concatenate...", "  title=[Concatenated Stacks] image1=[Concatenated Stacks] image2=mask image3=[-- None --]");
				}
				selectImage("Lipids");
			} 
			selectImage("Lipids");
			run("Close");
			selectImage("Concatenated Stacks");			
        	rename("Lipids");
   




            selectWindow("C3-"+root);            
            run("8-bit");        
            run("Merge Channels...", "c1=Lipids c2=Plasts c4=C3-"+root+" create ignore");			
            run("Stack to RGB", "slices keep");
            saveAs("Tiff", path+"results/Images4J/"+list[ii]+"_Incrust3D.tif");
            selectWindow("temp2");  
        	Stack.getDimensions(width,height,channels,slices,frames);
			for (yi = 1; yi < slices+1; yi++) {
				Stack.setSlice(yi);
				setThreshold(1, 255);
				run("Create Mask");
				if (yi == 1) {
					mask = getImageID();
					selectImage(mask);
					rename("maskStack");
				} else if (yi == 2) {
					selectImage(mask);
					run("Concatenate...", "  title=[Concatenated Stacks] image1=maskStack image2=mask image3=[-- None --]");
				} else {
					run("Concatenate...", "  title=[Concatenated Stacks] image1=[Concatenated Stacks] image2=mask image3=[-- None --]");
				}
				selectImage("temp2");
			} 
			selectImage("temp2");
			run("Close");
			selectImage("Concatenated Stacks");		
			rename("temp2");


            //run("Invert","stack");
            run("RGB Color");
            imageCalculator("Min create stack", list[ii]+"_Incrust3D.tif","temp2");
            saveAs("Tiff", path+"results/Images4J/"+list[ii]+"_Incrust3D_Solo.tif");
            run("Close All");
       
   






            wait(200);
            if (isOpen("Tableau_Resultats")){
               selectWindow("Tableau_Resultats");
               IJ.renameResults("Results");
            }
            // on passe sur chaque algaes et on inscrit les différentes valeurs
            for (i=0; i<nSelectionPerso; i++) {
                                // Excel Export n'aime pas les espaces
                setResult("Label", Nbalgaes,replace(list[ii], " ","_"));
                setResult("algae",Nbalgaes,(i+1));
                setResult("Volume_algae",Nbalgaes,d2s(volumeC3[i],1));
                setResult("Volume_Plasts",Nbalgaes,d2s(volumeC2[i],1));
                setResult("Volume_Lipids",Nbalgaes,d2s(volumeC1[i],1));
                Nbalgaes++;
            }
            updateResults(); 
            selectWindow("Results");
            IJ.renameResults("Tableau_Resultats"); 

        }
    }

    selectWindow("Tableau_Resultats");
    IJ.renameResults("Results");
    saveAs("Results", path+"results/Results.csv");
    // Use only if plugin is included in your fiji
    //run("Excel...", "select...="+path+"results/Results.xls");
    setBatchMode(false);
    waitForUser("Information", "End of procedure");
}
